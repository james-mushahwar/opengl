#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include <fstream>
#include <string>
#include <sstream>

#include "Renderer.h"

#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "Shader.h"
#include "Texture.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

int main(void)
{
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(PIXEL_WIDTH, PIXEL_HEIGHT, "OpenGL Hollow Knight", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    glfwSwapInterval(1);

    if (glewInit() != GLEW_OK) // needs to be called after valid context window
    {
        std::cout << "Error in glewinit" << std::endl;
    }

    std::cout << glGetString(GL_VERSION) << std::endl;

    {
        float positions[] = {
			 -50.0f, -50.0f, 0.0f, 0.0f, // vertex 0
			  50.0f, -50.0f, 1.0f, 0.0f, //  vertex 1
			  50.0f,  50.0f,  1.0f, 1.0f, // vertex 2
			 -50.0f,  50.0f, 0.0f, 1.0f, // vertex 3
        };

        unsigned int indices[] = {
            0, 1, 2,
            2, 3, 0
        };

        GLCall(glEnable(GL_BLEND));
        GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));  // enable blend the 1 minus the source (image) alpha

        VertexArray va;
        VertexBuffer vb(positions, 4 * 4 * sizeof(float));

        VertexBufferLayout layout;
        layout.Push<float>(2);
        layout.Push<float>(2);
        va.AddBuffer(vb, layout);

        IndexBuffer ib(indices, 6);
        va.Bind();
        ib.Bind();

        glm::mat4 proj = glm::ortho(0.0f, PIXEL_WIDTH, 0.0f, PIXEL_HEIGHT, -1.0f, 1.0f); // orthographic 4 by 3 aspect ratio matrix
        glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)); //view matrix

        Shader shader("res/shaders/Basic.shader");
        shader.Bind();

		Texture texture("res/textures/hollowknight.png");
		texture.Bind();
        shader.SetUniform1i("u_Texture", 0);

        va.Unbind();
        ib.Unbind();
        shader.Unbind();

        Renderer renderer;

        // gui setup
        IMGUI_CHECKVERSION();
        ImGui::CreateContext(); 
        ImGuiIO& io = ImGui::GetIO(); (void)io;
		// Setup Dear ImGui style
        ImGui::StyleColorsDark();
		// Setup Platform/Renderer backends
        ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init("#version 330 core");

			// Our state
		bool show_demo_window = true;
		bool show_another_window = false;
		ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

        glm::vec3 translationA(200.0f, 200.0f, 0.0f);
        glm::vec3 translationB(350.0f, 200.0f, 0.0f);
        float alpha = 1.0f;

        float g = 0.0f;
        float increment = 0.05f;
        /* Loop until the user closes the window */
        while (!glfwWindowShouldClose(window))
        {
            /* Render here */
            renderer.Clear();

            glfwPollEvents();
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

            shader.Bind();
            {
			    glm::mat4 model = glm::translate(glm::mat4(1.0f), translationA); //model matrix
			    glm::mat4 mvp = proj * view * model; // model view projection
                shader.SetUniformMat4f("u_MVP", mvp);
                renderer.Draw(va, ib, shader);
            }

            {
                glm::mat4 model = glm::translate(glm::mat4(1.0f), translationB); //model matrix
                glm::mat4 mvp = proj * view * model; // model view projection
                shader.Bind();
                shader.SetUniformMat4f("u_MVP", mvp);
                renderer.Draw(va, ib, shader);
            }
            
            shader.SetUniform1f("u_alpha", alpha);


            if (g > 1.0f)
                increment = -0.05f;
            else if (g < 0.0f)
                increment = 0.05f;

            g += increment;
            
			{
				static float f = 0.0f;
				static int counter = 0;

				ImGui::Begin("Transform");                          // Create a window called "Hello, world!" and append into it.

				//ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
				//ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
				//ImGui::Checkbox("Another Window", &show_another_window);
                ImGui::Text("Object A");
				ImGui::SliderFloat("TranslationA X", &translationA.x, 0.0f, PIXEL_WIDTH);
				ImGui::SliderFloat("TranslationA Y", &translationA.y, 0.0f, PIXEL_HEIGHT); 
				ImGui::SliderFloat("TranslationA Z", &translationA.z, -1.0f, 1.0f);
                ImGui::Text("Object B");
                ImGui::SliderFloat("TranslationB X", &translationB.x, 0.0f, PIXEL_WIDTH);
                ImGui::SliderFloat("TranslationB Y", &translationB.y, 0.0f, PIXEL_HEIGHT);
                ImGui::SliderFloat("TranslationB z", &translationB.z, -1.0f, 1.0f);

                ImGui::Text("Colour channels");
				ImGui::SliderFloat("Global alpha channel ", &alpha, 0.0f, 1.0f); 


				//ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

				//if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
				//	counter++;
				//ImGui::SameLine();
				//ImGui::Text("counter = %d", counter);

				ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
				ImGui::End();
			}

            ImGui::Render();
			/*int display_w, display_h;
			glfwGetFramebufferSize(window, &display_w, &display_h);
			glViewport(0, 0, display_w, display_h);
			glClearColor(clear_color.x* clear_color.w, clear_color.y* clear_color.w, clear_color.z* clear_color.w, clear_color.w);
			glClear(GL_COLOR_BUFFER_BIT);*/
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            /* Swap front and back buffers */
            glfwSwapBuffers(window);

            /* Poll for and process events */
            glfwPollEvents();
        }
    }
	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
    glfwTerminate();
    return 0;
}