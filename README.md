# OpenGl

My first OpenGL C++ project.

**Overview**

Here is a journal and walkthrough of the progess I have made while learning OpenGL (C++). This project is a personal project which also coincides with my MProf Games Development course at Abertay University, where I chose to do this project as part of the Critical Skills Development (DES507) module. this module allowed me to identify an area that I wanted to learn/improve and so this was all my own self-guided learning.

I had many reasons behind wanting to do this. Not only was I deeply interested in Graphics porgramming on a high-level, I wanted to improve my skills and broaden my programming options and potential career pathways.

**Why is this important to me?**
-	I will learn through practice how to make a C++ project from scratch, instead of using what is already pre-made in an engine.
-	I will learn more about the tools used during C++ development such as debugging and memory debugging.
-	I will be using more code that I am unfamiliar with which will improve my ability to approach new code that I initially don’t understand.

Below I document the steps I took and the important moments I learned throughout my journey of learning OpenGL. Some steps are simplified to avoid over explaining the same steps. I also comment on what certain code snippets mean, or how my final image (rendered image screenshot) came about.

**My learning journey**

**Part 1- Rendering a basic triangle**

First I start with setting up the OpenGL project. In summary I successfully got my program **rendering a triangle every frame**. I did this both ways; one with **old OpenGL** and with **Modern OpenGL**. 

The main code loop is as follows:
```
int main(void)
{
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK) // needs to be called after valid context window
    {
        std::cout << "Error in glewinit" << std::endl;
    }

    std::cout << glGetString(GL_VERSION) << std::endl;

    float positions[6] = {
       -0.5f, -0.5f,
        0.0f,  0.5f,
        0.5f, -0.5f
    };

    // vertex buffers and drawing a triangle tutorial
    unsigned int buffer;
    glGenBuffers(1, &buffer); // generate vertex buffer and get id assigned to &buffer
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), positions, GL_STATIC_DRAW);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        static float x_shift = 0;
        glClear(GL_COLOR_BUFFER_BIT);

        glDrawArrays(GL_TRIANGLES, 0, 3); // draws triangle from the currently bound buffer - e.g. from glBindBuffer
        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
```

The code above, in general, does the following:
- Sets up a window are the current context to render to 640 px wide and 480 px tall.
- Set up glew (extension library for OpenGL).
- Created an array of 6 floats. Here, these floats represent 3 vertices which represent 3 coordinates relative to the context window. (0,0) represents the centre of the context window. (-1,-1) is the bottom-left of the context window, and (1,1) is the top-right.
- Generate a buffer `(glGenBuffers)`, bind the buffer id as an Array buffer glBindBuffer(GL_ARRAY_BUFFER, buffer); and then specify the length of the data in the array (6 floats) and the array to point to (`positions`).
- Within the while loop the colour buffer is cleared from the previous frame, then glDrawArrays draws the primitives (GL_TRIANGLES) using the bound array buffer we set up before.

![Basic triangle](readmeImages/opengl white triangle.png) 


**Part 2- Using Index Buffers to create a Rectangle**

Next I shifted my shader to Basic.shader and loaded both the vertex and fragment shader at run-time. Then I introduced Index Buffers. Index Buffers are used to prevent the duplication of vertices in an object, for example, an .fbx model or even a rectangle. To draw a rectangle without index buffers there will have to be 6 vertices used (x2 Triangles), but we can see that these vertices where the two triangles meet will be duplicated which causes a waste in memory. By using Index Buffers we're able to define the 6 indexes we want to use in an array of only 4 unique vertices. So we have reduced our vertices count from 6 to 4, which is a significant reduction in memory used. This is important to use, especially in models with a high poly-count.

Here is an example of what my vertices and index arrays became:

```
float positions[] = {
			 -0.5f, -0.5f,  // vertex 0
			  0.5f, -0.5f,  //  vertex 1
			  0.5f,  0.5f,  // vertex 2
			 -0.5f,  0.5f,  // vertex 3
        };

        unsigned int indices[] = {
            0, 1, 2,
            2, 3, 0
        };
```
In `indices` we have the first triangle on the first line, which references the 3 vertices in `positions` (vertex 0, 1 and 2). Then in the second line we reference vertex 2, 3, and 0 to create a rectangle (square in this case).

Then with my shader I utilised Uniforms to send a changing colour vector4 to my fragment shader which produced the following glowing rectangle.

![Basic rectangle pulsing](readmeImages/pulsing rectangle.gif) 


**Part 3- Using Vertex Array objects and abstracting my code so far**

This next phase was about abstracting my program out into classes so that the main() function in Application.cpp was a lot cleaner. This allowed me to organise my thinking and understand more about what the purpose of each class should be, such as the Index Buffer class, Vertex Buffer class and Vertex Array class. 

I was also influenced by the images I saw on LearnOpenGL where Vertex array objects and vertex buffer objects, which hold the attributes for each vertices, were very well explained. 

![LearnOpenGL: Vertex Array object](readmeImages/vertex_array_objects.png) 

The second image shows how the index buffer can also be bound to the Vertex Array object and this can simplify the set-up of drawing various models and referencing various bound attribute locations in my vertex and fragment shaders.

![LearnOpenGL: Vertex Array object with Element buffer object](readmeImages/vertex_array_objects_ebo.png) 

The overview of my vertex array, vertex buffer, layout of attributes, and index buffers look like the following:

Application.cpp:
```
VertexArray va;
VertexBuffer vb(positions, 4 * 4 * sizeof(float));

VertexBufferLayout layout;
layout.Push<float>(2);
layout.Push<float>(2);
va.AddBuffer(vb, layout);

IndexBuffer ib(indices, 6);
va.Bind();
ib.Bind();
```

Above the three separate sections represent the following:
1. First, I create a Vertex array object (with an id, `m_RendererID`), and call `glGenVertexArrays(1, &mRenderer_ID)` in the `VertexArray` class constructor. The next line is the creation of my `VertexBuffer vb`, which defines what vertices I'm using (`positions`) and the size of this entire vertex buffer (4 lines of 4 floats).
2. Then I create a `VertexBufferLayout` class which hold a list of the number of attributes within each vertex. Here, each vertex is made up of 2 floats to represent the (x, y) coordinates of the vertex and 2 floats to represent the relative texture coordinates (not seen yet). The `VertexArray` (`va`) then adds the vertexbuffer and the attribtues we have defined in the `VertexBufferLayout`.
3. Lastly, the `IndexBuffer` is created and references the `indices` array we have seen previously. The index buffer is bound so that when the draw call is made the renderer knows which vertices to index to and how many vertices to iterate through before drawing is complete.


**Part 4- Textures**

Before, in Part 3, I mentioned how each vertices had a texture coordinate, which we hadn't seen before. This was used for my next step in learning more about OpenGL rendering. I downloaded a Hollow Knight PNG to use as a test. I then loaded and binded the PNG to the same rectangle that I had been using so far, and altered my Basic.shader to pass the texture coordinates as an output from my vertex shader, then into my fragment shader. In my fragment shader I sampled the texture relative to the position of the `v_TexCoord` in order to determine what each pixel should be coloured based on the loaded texture/PNG.
```
layout(location = 0) out vec4 colour;

in vec2 v_TexCoord; // takes in from the vertex shader

uniform sampler2D u_Texture;

void main()
{
    vec4 texColor = texture(u_Texture, v_TexCoord);
    colour = texColor;
};
```
And here is an updated look at the vertices array showing the relative sizes (from 0.0 to 1.0) of the texture coordinates:
```
float positions[] = {
			 -50.0f, -50.0f, **0.0f, 0.0f**, // vertex 0
			  50.0f, -50.0f, **1.0f, 0.0f**, //  vertex 1
			  50.0f,  50.0f, **1.0f, 1.0f**, // vertex 2
			 -50.0f,  50.0f, **0.0f, 1.0f**, // vertex 3
};
```
Here is my rendered Hollow Knight image:

![Hollow knight basic render](readmeImages/opengl ortho projection.png) 


**Part 5- Model View Projection**

Using **glm** (a library used for matrices) I then started to learn about _Model View Projection_ where I mapped my vertices from -1 to 1 in x and y, relative to the screen's width and height, where (0,0) represents the centre of the camera's orthographic projection. ` glm::mat4 proj = glm::ortho(0.0f, PIXEL_WIDTH, 0.0f, PIXEL_HEIGHT, -1.0f, 1.0f);`.

I then passed this `proj` matrices into the vertex shader to map the position of the model in the world space to a screen space based on the projection of the camera. 

```
shader.SetUniformMat4f("u_MVP", proj);
```

**//in basic.shader vertex shader**
```
#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 texCoord;

out vec2 v_TexCoord;

**uniform mat4 u_MVP;**

void main()
{
   **gl_Position = u_MVP * position;**
   v_TexCoord = texCoord;
};
```

I then wanted some interaction with the rendered image so I used a GUI library called **ImGui**. I then added transform values to the gui for x,y and z as well as an alpha channel scale which is passed into the fragment shader and determines the opacity of all objects and the output buffer of our fragment shader which is then blended with the target buffer.

![Hollow knight gui translation](readmeImages/Translating model.gif) 


**Part 6- Multiple object rendering**

To render two or more objects, I used the same uniform variable `u_MVP` but set the value twice in my while loop to represent the two different translations of these objects.
```
glm::vec3 translationA(200.0f, 200.0f, 0.0f);
glm::vec3 translationB(350.0f, 200.0f, 0.0f);

…

shader.Bind();
            {
	glm::mat4 model = glm::translate(glm::mat4(1.0f), translationA); //model matrix
	 glm::mat4 mvp = proj * view * model; // model view projection
                shader.SetUniformMat4f("u_MVP", mvp);
                renderer.Draw(va, ib, shader);
            }

            {
                glm::mat4 model = glm::translate(glm::mat4(1.0f), translationB); //model matrix
                glm::mat4 mvp = proj * view * model; // model view projection
                shader.Bind();
                shader.SetUniformMat4f("u_MVP", mvp);
                renderer.Draw(va, ib, shader);
            }
```

![Hollow knight multiple objects rendered](readmeImages/OpenGL ortho view.gif) 

I learned about _Batching_- which is an efficient way to draw multiple objects in one vertex buffer, using one draw call. Changing the uniform like this for every object can be slow because of the time taken to send a value from the cpu to the gpu.


**What I plan to do for the future**

After learning about Batching and the performance boost this can provide to real-time rendering, I want to demonstrate this next in my project. I am also currently reading through "Real-time rendering" 4th edition, which provides an in-depth overview into so many topics that I am looking to explore.


Thank you for taking the time to learn about my development journey into OpenGL so far. :)
